package ru.nortti.filmssearch

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.exit_dialog.view.*


class MainActivity : AppCompatActivity() {

    private val filmViewModel by lazy { ViewModelProviders.of(this).get(FilmViewModel::class.java)}
    private var layoutManager = GridLayoutManager(this, 2)
    private var position = 1
    private var color = 0
    companion object {
        private const val TAG = "MainActivity"
        private const val RESULT_CODE = 0
        private const val DAY = 1
        private const val NIGHT = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        position = intent.getIntExtra("position", 1)
        when (position) {
            DAY -> {
                setTheme(R.style.AppTheme_Day)

            }
            NIGHT -> {
                setTheme(R.style.AppTheme_Night)

            }
            else -> {
            }
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                layoutManager =  GridLayoutManager(this, 2) }
            else {
                layoutManager = GridLayoutManager(this, 4)
            }


        var adapter = FilmsAdapter(this)
        rvList.layoutManager = layoutManager
        rvList.adapter = adapter

        filmViewModel.getListFilms().observe(this, Observer {
            it?.let {
                adapter.addFilms(it)
            }
        })

        adapter.setOnClickListener(object : FilmsAdapter.OnClickListener {
            override fun onClick(item: Film) {
                val intent = Intent(this@MainActivity, DetailsActivity::class.java).apply {
                    putExtra("film", item)
                    putExtra("position", position)
                }
                startActivityForResult(intent, RESULT_CODE)
            }

        })

        adapter.setStyle(position)

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, String.format("Is checked: %s",  data!!.getBooleanExtra(DetailsActivity.CHECKED, false)))
                Log.d(TAG, String.format("Comment: %s",  data!!.getStringExtra(DetailsActivity.COMMENT)))
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
//            R.id.locale -> {
//
//                return true
//            }
            R.id.theme -> {
                if (position == 1) { position = 2 } else { position = 1 }
                reload(position)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun reload(pos: Int) {
        finish();
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("position", pos);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onBackPressed() {


        val dialog = AlertDialog.Builder(this)
        val viewLayout = LayoutInflater.from(this).inflate(R.layout.exit_dialog, null)
        dialog.setView(viewLayout)
        val alertDialog = dialog.create()
        viewLayout.yes.setOnClickListener {
            finish()
        }
        viewLayout.no.setOnClickListener(View.OnClickListener { alertDialog.dismiss() })
        alertDialog.show()

    }

}
