package ru.nortti.filmssearch

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_details.*


class DetailsActivity : AppCompatActivity() {

    var film: Film = Film()
    var position = -1
    companion object {
        private const val TAG = "DetailsActivity"
        const val CHECKED = "is_checked"
        const val COMMENT = "comment"
        private const val DAY = 1
        private const val NIGHT = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        film = intent.getParcelableExtra("film")!!
        position = intent.getIntExtra("position", 1)
        when (position) {
            DAY -> {
                setTheme(R.style.AppTheme_Day)
            }
            NIGHT -> {
                setTheme(R.style.AppTheme_Night)
            }
            else -> {
            }
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)



        initViews()

        val drawableResourceId: Int = getResources()
            .getIdentifier(film.image, "drawable", getPackageName())
        Glide
            .with(this)
            .load(drawableResourceId)
            .into(poster)


        titleTx.text = film.name
        descriptionTx.text = film.desctiption

        shareBut.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, film.name)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(CHECKED, likeCheckbox.isChecked)
        intent.putExtra(COMMENT,comment.text.toString())
        setResult(Activity.RESULT_OK, intent)

        super.onBackPressed()
    }

    fun initViews() {
        when (position) {
            DAY -> {
                titleTx.setTextColor(Color.GRAY)
                descriptionTx.setTextColor(Color.GRAY)
                lay.setBackgroundColor(Color.WHITE)
                likeCheckbox.setTextColor(Color.GRAY)
                comment.setHintTextColor(Color.GRAY)
            }
            NIGHT -> {
                titleTx.setTextColor(Color.WHITE)
                descriptionTx.setTextColor(Color.WHITE)
                lay.setBackgroundColor(Color.GRAY)
                likeCheckbox.setTextColor(Color.WHITE)
                comment.setHintTextColor(Color.WHITE)
            }
            else -> {
            }
        }

    }

}
